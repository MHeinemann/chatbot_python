from flask import Flask, render_template, request

import random

app = Flask(__name__)

zufallsantworten=["Oh, wirklich", "Interessant ...", "Das kann man so sehen", "Ich verstehe ..."]

reaktionsantworten = {"hallo": "aber Hallo", 
					  "geht": "Was verstehst du darunter?", 
					  "essen": "Ich habe leider keinen Geschmackssinn :("
					  }

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/get")
def get_bot_response():
    bot_response = ""
    userText = request.args.get('msg')

    userText = userText.lower()
    userText = userText.split()

    intelligenteAntworten = False
    for einzelwoerter in userText:
        if einzelwoerter in reaktionsantworten:
            bot_response = reaktionsantworten[einzelwoerter]
            intelligenteAntworten = True
    if intelligenteAntworten == False:
        bot_response = random.choice(zufallsantworten)

    return bot_response

if __name__ == "__main__":
    app.run()