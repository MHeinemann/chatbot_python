import time
import re

stage = 0

stage_questions = ["Was sind ihre Lieblingstiere?", "Was ist Kaputt?"]

reaktionsantworten_1 = {"hund": "Ich empfehle einen Züchter.",
                        "katze": "Da würde ich an ihrer Stelle beim Tierheim nachfragen.",
                        "maus": "Die finden sie auf der Straße."
                        }

reaktionsantworten_2 = {"software": "aber Hallo",
                        "pc": "Was verstehst du darunter?",
                        "privat": "Ich habe leider keinen Geschmackssinn :(",
                        "laptop": "Ich empfehle einen Züchter."
                        }

stage_answers = [reaktionsantworten_1, reaktionsantworten_2]


def check(string, regex):
    if (re.fullmatch(regex, string)):
        return True
    else:
        return False


def createTicket():
    email_regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

    print("Okey ich kümmer mich um alles! Wir müssen nur eben ihre Daten aufnehmen.")
    time.sleep(1)
    print("Wie heißen sie?")
    name = input("Ihr Name: ")

    print("Wie ist ihre Kontaktmail")
    email = input("Ihre E-Mail: ")
    while not check(email, email_regex):
        print("Das ist leider keine korrekte E-Mail")
        email = input("Ihre E-Mail: ")


while True:

    if stage == 0:
        print("Willkommen beim Chatbot")
        print("")
        time.sleep(1)

    print(stage_questions[stage])
    time.sleep(0.5)
    print("Sie haben folgende Möglichkeiten zur Antwort:")
    for key, value in stage_answers[stage].items():
        time.sleep(0.3)
        print(key)

    nutzereingabe = ""
    while nutzereingabe == "":
        nutzereingabe = input("Ihre Antwort: ")

    nutzereingabe = nutzereingabe.lower()

    if nutzereingabe in stage_answers[stage]:
        print(stage_answers[stage][nutzereingabe])
    else:
        print("Leider habe ich darauf keine Antwort.")
        print("Ihre Möglichkeiten sind:")
        for key, value in stage_answers[stage].items():
            print(key)

    stage += 1

    if stage == 2:
        time.sleep(0.5)
        createTicket()
        time.sleep(0.5)
        print("Alles klar, tschüss!")
        break
