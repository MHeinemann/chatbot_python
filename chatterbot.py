from chatterbot import ChatBot

from chatterbot.trainers import ListTrainer  # method to train the chatbot

bot = ChatBot('MyChatBot')
bot.set_trainer(ListTrainer)

conversation = open('chats.txt','r').readlines()

bot.train(conversation)

name =  input("Wie darf ich dich nennen?")


while True:
    message = input(name + ':')
    if message.strip() == 'Bye' or message.strip() == 'bye':
        print('ChatBot:Bye')
        break
    else: 
        reply = bot.get_response(message)
        print('ChatBot:',reply)